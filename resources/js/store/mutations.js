import * as types from './mutation-types'
import axios from 'axios'

export default {
    [types.LOGIN]( state ) {
        state.logged = 1;
    },
    [types.LOGOUT]( state ) {
        state.logged = 0;
        state.user = {};
    },
    [types.ADMIN]( state ) {
        state.role = "admin"
    },
    [types.INSTRUCTOR]( state ) {
        state.role = "instructor"
    },
    [types.SUADMIN]( state ) {
        state.role = "suadmin"
    },
    [types.MGR]( state ) {
        state.role = "mgr"
    },
    [types.EMPLOYER]( state ) {
        state.role = "employer"
    },
    [types.CLEAR]( state ) {
        state.role = ""
    },
    [types.SETID]( state, payload ) {
        state.userID = payload
    },
    [types.GETUSER]( state, payload ) {
        state.user = payload

    }
}