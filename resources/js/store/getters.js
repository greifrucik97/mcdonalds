export default {
    logged(state) {
        return state.logged
    },
    role(state) {
        return state.role
    },
    isSuAdmin(state) {
        return state.role === "suadmin"
    },
    isAdmin(state) {
        return state.role === "admin" || state.role === "suadmin"
    },
    isMgr(state) {
        return state.role === "mgr" || state.role === "admin" || state.role === "suadmin"
    },
    isInstructor(state) {
        return state.role === "instructor" || state.role === "mgr" || state.role === "admin" || state.role === "suadmin"
    },
    isEmployer(state) {
        return state.role === "employer" || state.role === "instructor" || state.role === "mgr" || state.role === "admin" ||  state.role === "suadmin"
    },
    userID(state) {
        return state.userID
    },
    getUser(state) {
        return state.user
    }
}