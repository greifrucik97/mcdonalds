import * as types from './mutation-types'

export default {
    [types.LOGIN]({ commit }) {
        commit(types.LOGIN)
    },
    [types.LOGOUT]({ commit }) {
        commit(types.LOGOUT)
    },
    [types.ADMIN]({ commit }) {
        commit(types.ADMIN)
    },
    [types.MGR]({ commit }) {
        commit(types.MGR)
    },
    [types.EMPLOYER]({ commit }) {
        commit(types.EMPLOYER)
    },
    [types.INSTRUCTOR]({ commit }) {
        commit(types.INSTRUCTOR)
    },
    [types.SUADMIN]({ commit }) {
        commit(types.SUADMIN)
    },
    [types.CLEAR]({ commit }) {
        commit(types.CLEAR)
    },
    [types.SETID]({ commit }, payload) {
        commit(types.SETID, payload)
    },
    [types.GETUSER]({ commit }, payload) {
        commit(types.GETUSER, payload)
    },
}