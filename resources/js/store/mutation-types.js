export const LOGIN = "LOGIN"
export const  LOGOUT = "LOGOUT"
export const EMPLOYER = "EMPLOYER"
export const MGR = "MGR"
export const SUADMIN = "SUADMIN"
export const INSTRUCTOR = "INSTRUCTOR"
export const ADMIN = "ADMIN"
export const CLEAR = "CLEAR"
export const SETID = "SETID"
export const GETUSER = "GETUSER"