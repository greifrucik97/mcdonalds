import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Notifications from 'vue-notification'
import jwtDecode from 'jwt-decode'
import { get } from 'lodash'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import SmartTable from 'vuejs-smart-table'
import Vuelidate from 'vuelidate'

//require("../assets/js/laravel-echo-setup");



import 'es6-promise/auto'

Vue.use(VueAxios, axios);
Vue.use(Notifications);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(SmartTable);
Vue.use(Vuelidate)


import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import * as types from './store/mutation-types'


Vue.axios.interceptors.request.use(config => {
    if(localStorage.getItem('token')) {
        config.headers['Authorization'] = localStorage.getItem('token')
    }
    config.headers['Content-Type'] = 'application/json';
    return config;
});

Vue.axios.interceptors.response.use(response => {
    const newtoken = get(response, 'headers.authorization')
    if (newtoken) {
        localStorage.setItem('token', newtoken)
    }

    if (response.status !== 200) {
        store.dispatch(types.LOGOUT);
        store.dispatch(types.CLEAR);
        router.push("/");
    }

    return response
});

router.beforeEach((to, from, next) => {
    let roleNumber = 0;
    if (localStorage.getItem('token')) {
        let token = localStorage.getItem('token');
        let decodeToken = jwtDecode(token);
        store.dispatch(types.SETID, decodeToken.user.id)
        store.dispatch(types.GETUSER, decodeToken.user)
        switch (decodeToken.user.role) {
            case "suadmin":
                roleNumber=50
                store.dispatch(types.SUADMIN)
                break;
            case "admin":
                roleNumber=40
                store.dispatch(types.ADMIN)
                break;
            case "mgr":
                roleNumber=30
                store.dispatch(types.MGR)
                break;
            case "instructor":
                roleNumber=20
                store.dispatch(types.INSTRUCTOR)
                break;
            case "employer":
                roleNumber=10
                store.dispatch(types.EMPLOYER)
                break;
            default:
                roleNumber=0
                break;
        }
        store.dispatch(types.LOGIN)
    }


    if (to.meta.role === "GUEST") {
        next()
    } else {
        if (to.meta.role === "SUADMIN") {
            if (roleNumber >= 50 ) {
                next()
            } else {
                next('/')
            }
        } else if (to.meta.role === "ADMIN") {
            if (roleNumber >= 40 ) {
                next()
            } else {
                next('/')
            }
        } else if (to.meta.role === "MGR") {
            if (roleNumber >= 30 ) {
                next()
            } else {
                next('/')
            }
        } else if (to.meta.role === "INSTRUCTOR") {
            if (roleNumber >= 20 ) {
                next()
            } else {
                next('/')
            }
        } else if (to.meta.role === "EMPLOYER") {
            if (roleNumber >= 10) {
                next()
            } else {
                next('/')
            }
        } else {
            next('/')
        }
    }
})


const app = new Vue({
    el: '#app',
    components: { App },
    router,
    store
});