import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from "../Views/Login";
import Account from "../Views/Account";
import Register from "../Views/Register";

import Admin from "../Views/Admin";
import AdminSites from "../Views/Admin/AdminSites";
import SettingsAccount from "../Views/Settings/SettingsAccount";
import Settings from "../Views/Settings";
import SettingsChangePassword from "../Views/Settings/SettingsChangePassword";
import AdminUsers from "../Views/Admin/AdminUsers";
import GraphicMonth from "../Views/GraphicMonth";
import AdminGraphic from "../Views/Admin/AdminGraphic";

Vue.use(VueRouter);



const routes = [
    {
        path: '/',
        name: 'login',
        component: Login,
        meta: {
            role: 'GUEST'
        }
    },
    {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            role: 'GUEST'
        }
    },
    {
        path: '/user',
        name: 'user',
        component: Account,
        meta: {
            role: 'EMPLOYER'
        }
    },
    {
        path: '/user/msc',
        name: 'GraphicMonth',
        component: GraphicMonth,
        meta: {
            role: 'EMPLOYER'
        }
    },
    {
        path: '/settings',
        name: 'settings',
        component: Settings,
        meta: {
            role: 'EMPLOYER'
        },
        children: [
            {
                path: '/settings/account',
                component: SettingsAccount,
                name: 'SettingsAccountComponent',
                meta: {
                    role: 'EMPLOYER'
                },
            },
            {
                path: '/settings/changePassword',
                component: SettingsChangePassword,
                name: 'SettingsChangePasswordComponent',
                meta: {
                    role: 'EMPLOYER'
                },
            },
        ]
    },
    {
        path: '/admin',
        name: 'admin',
        component: Admin,
        meta: {
            role: 'ADMIN'
        },
        children: [
            {
                path: '/admin/users',
                component: AdminUsers,
                name: 'AdminUsers',
                meta: {
                    role: 'ADMIN'
                },
            },
            {
                path: '/admin/sites',
                component: AdminSites,
                name: 'AdminSites',
                meta: {
                    role: 'ADMIN'
                },
            },
            {
                path: '/admin/graphic',
                component: AdminGraphic,
                name: 'AdminGraphic',
                meta: {
                    role: 'ADMIN'
                },
            }
        ]
    }
];

const router = new VueRouter({
    mode: 'history',
    routes
});

export default router