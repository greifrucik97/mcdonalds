<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF Demo in Laravel 7</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <style>
        body {
            font-family: DejaVu Sans;
            font-size: 10px;
        }
    </style>
</head>
<body>
<h5 class="text-center">{{ $date }}</h5>
<table class="table table-sm table-bordered">
    <thead>
    <tr>
        <td>Dzień</td>
        <td>Start</td>
        <td>Koniec</td>
        <td>Liczba godzin</td>
        <td>Uwagi</td>
    </tr>
    </thead>
    <tbody>


    @foreach ($days as $data)
        <tr>
            <td>{{ $data['day'] }}</td>
            <td>{{ $data['start'] }}</td>
            <td>{{ $data['end'] }}</td>
            <td>{{ $data['numberHours'] }}</td>
            <td>{{ $data['attention'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>