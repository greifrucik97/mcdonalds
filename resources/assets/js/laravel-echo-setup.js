import Echo from 'laravel-echo';

window.io = require('socket.io-client');

window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: "localhost:6001",
    authEndpoint: "http://localhost:8000/broadcasting/auth",
    auth: {
        headers: {
            Authorization: localStorage.getItem('token'),
            Accept: 'application/json',
        },
    }
});