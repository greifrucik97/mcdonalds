<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



//DEFAULT ROUTES
Route::post('auth/login', 'AuthController@login');
Route::get('site/getSites', 'SiteController@getSites');
Route::post('user/registerUser', 'UserController@registerUser');


//SUADMIN ROUTES
Route::group([

    'middleware' => ['api', 'jwt.verify', 'role:suadmin'],

], function ($router) {

    Route::put('site/addSite', 'SiteController@addSite');
    Route::patch('site/editSite', 'SiteController@editSite');
    Route::delete('site/deleteSite', 'SiteController@deleteSite');

});

//ADMIN ROUTES
Route::group([

    'middleware' => ['api', 'jwt.verify', 'role:admin'],

], function ($router) {

    Route::post('updateGraphic', 'GraphicController@updateGraphic');
    Route::patch('user/activeUser', 'UserController@activeUser');
    Route::patch('user/unActiveUser', 'UserController@unActiveUser');
    Route::delete('user/deleteUser', 'UserController@deleteUser');
    Route::get('user/getUsers', 'UserController@getUsers');
    Route::patch('user/modifyUser', 'UserController@modifyUser');
    Route::patch('user/changeRole', 'UserController@changeRole');
    Route::post('day/synchronizeUserWithDay', 'DayController@synchronizeUserWithDay');
    Route::post('day/deleteOldDays', 'DayController@deleteOldDays');

    Route::put('monthPetition/createMonthPetition', 'MonthPetitionController@createMonthPetition');
    Route::patch('monthPetition/unActiveMonthPetition', 'MonthPetitionController@unActiveMonthPetition');
    Route::patch('monthPetition/activeMonthPetition', 'MonthPetitionController@activeMonthPetition');
    Route::patch('monthPetition/modifyMonthPetition', 'MonthPetitionController@modifyMonthPetition');

    Route::get('monthPetition/getMonthPetitions', 'MonthPetitionController@getMonthPetitions');
    Route::post('monthPetition/getMonthPetition', 'MonthPetitionController@getMonthPetition');
    Route::delete('monthPetition/deleteMonthPetitions', 'MonthPetitionController@deleteMonthPetitions');

});

//MGR ROUTES
Route::group([

    'middleware' => ['api', 'jwt.verify', 'role:mgr'],

], function ($router) {


});

//INSTRUCTOR ROUTES
Route::group([

    'middleware' => ['api', 'jwt.verify', 'role:instructor'],

], function ($router) {


});


//EMPLOYER ROUTES
Route::group([

    'middleware' => ['api', 'jwt.verify'],

], function ($router) {

    Route::post('auth/logout', 'AuthController@logout');
    Route::post('auth/refresh', 'AuthController@refresh');
    Route::post('auth/me', 'AuthController@me');

    Route::post('site/getSite', 'SiteController@getSite');

    Route::post('day/getGraphicForDay', 'DayController@getGraphicForDay');
    Route::post('day/getGraphicForUser', 'DayController@getGraphicForUser');
    Route::post('day/generatePdfForUser', 'DayController@generatePdfForUser');

    Route::patch('user/activeNotificationsUser', 'UserController@activeNotificationsUser');
    Route::patch('user/unActiveNotificationsUser', 'UserController@unActiveNotificationsUser');
    Route::post('user/getUser', 'UserController@getUser');
    Route::patch('user/changePassword', 'UserController@changePassword');

    Route::get('monthPetition/getActiveMonthPetitions', 'MonthPetitionController@getActiveMonthPetitions');


});
