<?php


namespace App\Helpers;

use App\Exceptions\DontHaveAccessToSite;

/**
 * Class UserHelper
 * @package App\Helpers
 */
class UserHelper
{

    //CONST
    public const ADMIN = 'admin';
    public const MGR = 'mgr';
    public const EMPLOYER = 'employer';
    public const INSRTUCTOR = 'instructor';
    public const SUADMIN = 'suadmin';

    //STATIC FUNCTIONS

    public static function isSuAdmin(){
        return auth()->user()->role == UserHelper::SUADMIN;
    }

    public static function isAdmin(){

        if ( auth()->user()->role == UserHelper::ADMIN || self::isSuAdmin())
        {
            return true;
        } else {
            return false;
        }

    }
    public static function isMgr(){

        if ( auth()->user()->role == UserHelper::MGR  || self::isAdmin() || self::isSuAdmin())
        {
            return true;
        } else {
            return false;
        }

    }

    public static function isInstructor(){

        if (auth()->user()->role == UserHelper::INSRTUCTOR || self::isMgr() || self::isAdmin() || self::isSuAdmin())
        {
            return true;
        } else {
            return false;
        }


    }

    public static function isEmployer(){

        if (auth()->user()->role == UserHelper::EMPLOYER || self::isInstructor() || self::isMgr() || self::isAdmin() || self::isSuAdmin())
        {
            return true;
        } else {
            return false;
        }

    }

    public static function getUserIdFromToken() {

        return auth()->user()->id;

    }

    public static function getUserFromToken() {

        return auth()->user();

    }

    /**
     * @param int|null $site_id
     * @throws DontHaveAccessToSite
     */
    public static function checkAccessToSite(?int $site_id) {
        if (!UserHelper::isSuAdmin() && $site_id != null) {
            $user = UserHelper::getUserFromToken();
            if ($user->site_id != $site_id) {
                throw new DontHaveAccessToSite();
            }
        }
    }


}