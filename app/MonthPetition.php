<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthPetition extends Model
{
    public function petitions()
    {
        return $this->belongsTo(Petition::class, 'month_petition_id');
    }
}
