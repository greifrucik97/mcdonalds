<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Petition extends Model
{
    public function monthPetition()
    {
        return $this->hasMany(MonthPetition::class);
    }
}
