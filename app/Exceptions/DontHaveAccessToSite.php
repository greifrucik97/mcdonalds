<?php

namespace App\Exceptions;

use Exception;

class DontHaveAccessToSite extends Exception
{
    protected $code = 422;
    protected $message = "Dont have access to site";
}
