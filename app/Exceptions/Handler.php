<?php
declare(strict_types=1);
namespace App\Exceptions;


use App\Http\Response\APIResponse;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use function var_export;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];


    /**
     * @param Request $request
     * @param Exception $exception
     * @return JsonResponse
     */
    public function render($request, Exception $exception): JsonResponse
    {
        if ($exception instanceof ValidationException) {
            return $this->renderException(
                new HttpException(422, $exception->getMessage()),
                422,
                ['error' => $exception->errors()]
            );
        }

        if ($exception instanceof DontHaveAccessToSite) {
            return $this->renderException(
                new HttpException(422, $exception->getMessage()),
                422,
                ['error' => 'DontHaveAccessToSite']
            );
        }

        return $this->renderException($exception);
    }

    private function getValidationErrorMessage(ValidationException $exception): string
    {
        $validator = $exception->validator;

        return var_export($validator->errors()->getMessages(), true);
    }

    /**
     * @param Exception $exception
     * @param int        $code
     * @param array|null $data
     * @return JsonResponse
     */
    private function renderException(Exception $exception, int $code = 0, ?array $data = null): JsonResponse
    {
        if (empty($code)) {
            $code = $this->isHttpException($exception) ? $exception->getStatusCode() : 500;
        }

        return APIResponse::produceResponse($code, $exception->getMessage(), $data);
    }
}
