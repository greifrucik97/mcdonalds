<?php

namespace App\Exceptions;

use Exception;

class DontHaveAccess extends Exception
{
    protected $code = 422;
    protected $message = "Dont have access";
}
