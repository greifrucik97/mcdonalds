<?php
namespace App\Http\Response;

use App\Exceptions\DontHaveAccessToSite;
use Illuminate\Http\JsonResponse;
use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;


class APIResponse
{
    private const STATUS = 'status';
    private const MESSAGE = 'msg';
    private const DATA = 'data';

    public static function produceResponseFromResponder(Responder $responder): JsonResponse
    {
        try {
            $returnedData = $responder->generate();
        } catch (Exception $e) {
            return self::produceResponseFromException($e);
        }

        return self::produceResponse(200, '', $returnedData);
    }


    public static function produceResponseFromException(Exception $exception): JsonResponse
    {
        $status = $exception->getCode() > 0 ? $exception->getCode() : 500;

        return self::produceResponse($status, $exception->getMessage());
    }

    public static function produceResponse(int $status, string $msg = '', ?array $data = null): JsonResponse
    {
        $returnArray = [
            self::STATUS => $status,
            self::MESSAGE => $msg,
        ];

        if ($data === null) {
            $returnArray[self::DATA] = [];
        } else {
            $returnArray[self::DATA] = $data;
        }

        $headers = ['Content-Type' => 'application/json; charset=utf-8'];

        return new JsonResponse($returnArray, $status, $headers);
    }

}
