<?php


namespace App\Http\Request\Day;


use App\Http\Request\RequestParametersModel;

/**
 * @property mixed month
 * @property mixed year
 */
class ForUserModel extends RequestParametersModel
{

    protected function parameterKeys(): array
    {
        return [
            self::YEAR_PARAM,
            self::MONTH_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::YEAR_PARAM => self::YEAR_RULE,
            self::MONTH_PARAM => self::MONTH_RULE
        ];
    }


}