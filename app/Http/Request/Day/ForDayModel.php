<?php


namespace App\Http\Request\Day;


use App\Http\Request\RequestParametersModel;

/**
 * @property mixed day
 * @property mixed month
 * @property mixed year
 * @property mixed site_id
 */
class ForDayModel extends RequestParametersModel
{

    protected function parameterKeys(): array
    {
        return [
            self::SITE_ID_PARAM,
            self::DAY_PARAM,
            self::YEAR_PARAM,
            self::MONTH_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::SITE_ID_PARAM => self::SITE_ID_NULLABLE_RULE,
            self::DAY_PARAM => self::DAY_RULE,
            self::YEAR_PARAM => self::YEAR_RULE,
            self::MONTH_PARAM => self::MONTH_RULE
        ];
    }


}