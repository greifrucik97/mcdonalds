<?php


namespace App\Http\Request\MonthPetition;


use App\Http\Request\RequestParametersModel;

class CreateMonthPetitionModel extends RequestParametersModel
{
    protected function parameterKeys(): array
    {
        return [
            self::NAME_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::NAME_PARAM => self::NAME_RULE
        ];
    }


}