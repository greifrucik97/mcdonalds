<?php


namespace App\Http\Request\MonthPetition;


use App\Http\Request\RequestParametersModel;

class ModifyMonthPetitionModel extends RequestParametersModel
{
    protected function parameterKeys(): array
    {
        return [
            self::MONTH_PETITIONS_ID_PARAM,
            self::NAME_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::MONTH_PETITIONS_ID_PARAM => self::MONTH_PETITIONS_ID_RULE,
            self::NAME_PARAM => self::NAME_RULE
        ];
    }


}