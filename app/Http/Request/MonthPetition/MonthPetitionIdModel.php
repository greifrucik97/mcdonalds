<?php


namespace App\Http\Request\MonthPetition;


use App\Http\Request\RequestParametersModel;

class MonthPetitionIdModel extends RequestParametersModel
{
    protected function parameterKeys(): array
    {
        return [
            self::MONTH_PETITIONS_ID_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::MONTH_PETITIONS_ID_PARAM => self::MONTH_PETITIONS_ID_RULE
        ];
    }


}