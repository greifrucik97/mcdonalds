<?php
namespace App\Http\Request;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as ValidatorFactory;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;
use function array_key_exists;


abstract class RequestParametersModel
{
    /**
     * @var array
     */
    private $attributes = [];


    /**
     * @var array
     */
    private $defaults;

    /**
     * @var Validator
     */
    private $validator;


    public const FIRSTNAME_PARAM = 'firstName';
    public const FIRSTNAME_RULE = 'string|required';

    public const LASTNAME_PARAM = 'lastName';
    public const LASTNAME_RULE = 'string|required';

    public const EMAIL_PARAM = 'email';
    public const EMAIL_RULE = 'string|required|unique:users,email';
    public const EMAIL_REMEMBER_PASSWORD_RULE = 'string|required|exists:users,email';
    public const EMAIL_SEND_LINK_RULE = 'required|string|unique:account_link_users|email:rfc,dns';

    public const OLD_PASSWORD_PARAM = 'oldPassword';
    public const OLD_PASSWORD_RULE = 'required|string|password:api';

    public const PASSWORD_PARAM = 'password';
    public const PASSWORD_RULE = [
        'required',
        'string',
        'min:8',             // must be at least 10 characters in length
        'regex:/[a-z]/',      // must contain at least one lowercase letter
        'regex:/[A-Z]/',      // must contain at least one uppercase letter
        'regex:/[0-9]/',      // must contain at least one digit
        'regex:/[@$!%*#?&]/', // must contain a special character
    ];

    public const PASSWORDCONFIRM_PARAM = 'passwordConfirm';
    public const PASSWORDCONFIRM_RULE = 'string|required|same:password';


    public const USER_ID_PARAM = 'user_id';
    public const USER_ID_RULE = 'numeric|required|exists:users,id';

    public const ROLE_PARAM = 'role';
    public const ROLE_RULE = 'string|required|in:employer,instructor,mgr,admin,suadmin';


    public const OWNER_ID_PARAM = 'owner_id';
    public const OWNER_ID_RULE = 'numeric|nullable|exists:users,id';
    public const OWNER_ID_REQUIRED_RULE = 'numeric|required|exists:users,id';

    public const SITE_ID_PARAM = 'site_id';
    public const SITE_ID_RULE = 'numeric|required|exists:sites,id';
    public const SITE_ID_NULLABLE_RULE = 'nullable|numeric|exists:sites,id';

    public const NAME_PARAM = 'name';
    public const NAME_RULE = 'string|required';

    public const NUMBER_PARAM = 'number';
    public const NUMBER_RULE = 'numeric|required';


    public const DAY_PARAM = 'day';
    public const DAY_RULE = 'required|numeric|between:1,31';

    public const YEAR_PARAM = 'year';
    public const YEAR_RULE = 'required|numeric|between:2019,2100';

    public const MONTH_PARAM = 'month';
    public const MONTH_RULE = 'required|numeric|between:1,12';

    public const GRAPHIC_PARAM = 'graphic';
    public const GRAPHIC_RULE = 'file|required|mimes:pdf';

    public const MGR_PARAM = 'mgr';
    public const MGR_RULE = 'file|required|mimes:pdf';

    public const MONTH_PETITIONS_ID_PARAM = 'month_petition_id';
    public const MONTH_PETITIONS_ID_RULE = 'required|numeric|exists:month_petitions,id';


    /**
     * RequestParametersModel constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->defaults = $this->defaultValues();
        $this->prepareAttributes($request);
        $this->forceDefault();
    }

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [];
    }

    /**
     * @return array
     */
    protected function defaultValues(): array
    {
        return [];
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return [];
    }

    /**
     * @return array
     */
    protected function messages(): array
    {
        return [];
    }

    /**
     * @return array
     */
    protected function forceDefaultRules(): array
    {
        return [];
    }


    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) var_export($this->attributes, true);
    }

    /**
     * @throws ValidationException
     */
    public function validate(): void
    {
        $this->validator = ValidatorFactory::make($this->attributes, $this->getRules(), $this->messages());
        $this->validator->validate();

    }

    /**
     * @param string $name
     * @return mixed
     * @throws Exception
     */
    public function __get(string $name)
    {
        if (!array_key_exists($name, $this->attributes)) {
            throw new Exception($name);
        }

        return $this->attributes[$name];
    }

    /**
     * @param string $name
     * @param $value
     */
    public function __set(string $name, $value): void
    {
        $this->attributes[$name] = $value;
    }

    /**
     * @param $name
     * @return bool
     */
    public function __isset($name): bool
    {
        if (isset($this->attributes[$name])) {
            return true;
        }

        return false;
    }

    /**
     * @param Request $request
     */
    private function prepareAttributes(Request $request): void
    {
        $keys = $this->parameterKeys();

        foreach ($keys as $key) {
            $this->attributes[$key] = $request->get($key, $this->getDefault($key));
        }
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    private function getDefault(string $key)
    {
        if (array_key_exists($key, $this->defaults)) {
            return $this->defaults[$key];
        }

        return null;
    }

    /**
     *
     */
    private function forceDefault(): void
    {
        $forceRules = $this->forceDefaultRules();

        foreach ($forceRules as $key => $values) {
            if (array_key_exists($key, $this->attributes)) {
                foreach ($values as $value) {
                    if ($value === $this->attributes[$key]) {
                        $this->attributes[$key] = $this->getDefault($key);
                        break;
                    }
                }
            }
        }
    }

    /**
     * @return array
     */
    private function getRules(): array
    {
        $rules = $this->rules();

        return $rules;
    }
}
