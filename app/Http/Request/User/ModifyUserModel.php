<?php


namespace App\Http\Request\User;


use App\Http\Request\RequestParametersModel;

/**
 * @property mixed user_id
 * @property mixed firstName
 * @property mixed lastName
 * @property mixed site_id
 */
class ModifyUserModel extends RequestParametersModel
{
    protected function parameterKeys(): array
    {
        return [
            self::USER_ID_PARAM,
            self::SITE_ID_PARAM,
            self::FIRSTNAME_PARAM,
            self::LASTNAME_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::USER_ID_PARAM => self::USER_ID_RULE,
            self::FIRSTNAME_PARAM => self::FIRSTNAME_RULE,
            self::LASTNAME_PARAM => self::LASTNAME_RULE,
            self::SITE_ID_PARAM => self::SITE_ID_RULE
        ];
    }


}