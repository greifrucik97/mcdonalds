<?php


namespace App\Http\Request\User;


use App\Http\Request\RequestParametersModel;

/**
 * @property mixed user_id
 * @property mixed password
 */
class ChangePasswordModel extends RequestParametersModel
{
    protected function parameterKeys(): array
    {
        return [
            self::USER_ID_PARAM,
            self::OLD_PASSWORD_PARAM,
            self::PASSWORD_PARAM,
            self::PASSWORDCONFIRM_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::OLD_PASSWORD_PARAM => self::OLD_PASSWORD_RULE,
            self::USER_ID_PARAM => self::USER_ID_RULE,
            self::PASSWORD_PARAM => self::PASSWORD_RULE,
            self::PASSWORDCONFIRM_PARAM => self::PASSWORDCONFIRM_RULE
        ];
    }


}