<?php


namespace App\Http\Request\User;


use App\Http\Request\RequestParametersModel;

/**
 * @property mixed user_id
 * @property mixed role
 */
class ChangeRoleModel extends RequestParametersModel
{
    protected function parameterKeys(): array
    {
        return [
            self::USER_ID_PARAM,
            self::ROLE_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::USER_ID_PARAM => self::USER_ID_RULE,
            self::ROLE_PARAM => self::ROLE_RULE,
        ];
    }


}