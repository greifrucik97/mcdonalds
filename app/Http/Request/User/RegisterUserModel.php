<?php


namespace App\Http\Request\User;


use App\Http\Request\RequestParametersModel;

/**
 * @property mixed firstName
 * @property mixed lastName
 * @property mixed password
 * @property mixed email
 * @property mixed site_id
 */
class RegisterUserModel extends RequestParametersModel
{
    protected function parameterKeys(): array
    {
        return [
            self::FIRSTNAME_PARAM,
            self::LASTNAME_PARAM,
            self::EMAIL_PARAM,
            self::PASSWORD_PARAM,
            self::PASSWORDCONFIRM_PARAM,
            self::SITE_ID_PARAM
        ];
    }


    protected function rules(): array
    {
        return [
            self::FIRSTNAME_PARAM => self::FIRSTNAME_RULE,
            self::LASTNAME_PARAM => self::LASTNAME_RULE,
            self::EMAIL_PARAM => self::EMAIL_PARAM,
            self::PASSWORD_PARAM => self::PASSWORD_RULE,
            self::PASSWORDCONFIRM_PARAM => self::PASSWORDCONFIRM_RULE,
            self::SITE_ID_PARAM => self::SITE_ID_RULE
        ];
    }


}