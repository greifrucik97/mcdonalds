<?php


namespace App\Http\Request\Site;


use App\Http\Request\RequestParametersModel;

/**
 * @property mixed site_id
 */
class SiteIDNullableModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::SITE_ID_PARAM
        ];
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return [
            self::SITE_ID_PARAM => self::SITE_ID_NULLABLE_RULE
        ];
    }


}