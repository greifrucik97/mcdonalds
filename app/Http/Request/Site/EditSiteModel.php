<?php


namespace App\Http\Request\Site;


use App\Http\Request\RequestParametersModel;

/**
 * @property mixed site_id
 * @property mixed name
 * @property mixed number
 */
class EditSiteModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::SITE_ID_PARAM,
            self::NUMBER_PARAM,
            self::NAME_PARAM
        ];
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return [
            self::SITE_ID_PARAM => self::SITE_ID_RULE,
            self::NUMBER_PARAM => self::NUMBER_RULE,
            self::NAME_PARAM => self::NAME_RULE
        ];
    }


}