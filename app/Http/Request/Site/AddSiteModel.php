<?php


namespace App\Http\Request\Site;


use App\Http\Request\RequestParametersModel;

/**
 * @property mixed name
 * @property mixed number
 */
class AddSiteModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::NUMBER_PARAM,
            self::NAME_PARAM
        ];
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return [
            self::NUMBER_PARAM => self::NUMBER_RULE,
            self::NAME_PARAM => self::NAME_RULE
        ];
    }


}