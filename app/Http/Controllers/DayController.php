<?php

namespace App\Http\Controllers;

use App\Http\Request\Day\ForDayModel;
use App\Http\Request\Day\ForUserModel;
use App\Http\Request\Site\SiteIDModel;
use App\Http\Request\Site\SiteIDNullableModel;
use App\Http\Resonders\Day\DeleteOldDaysResponder;
use App\Http\Resonders\Day\ForDayResponder;
use App\Http\Resonders\Day\ForUserResponder;
use App\Http\Resonders\Day\SynchronizeUserWithDayResponder;
use App\Http\Response\APIResponse;
use App\Http\Service\DayService;
use Barryvdh\DomPDF\PDF;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class DayController extends Controller
{

    public $dayService;

    /**
     * DayController constructor.
     */
    public function __construct()
    {
        $this->dayService = new DayService();
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function getGraphicForDay(Request $request): JsonResponse
    {
        $model = new ForDayModel($request);
        $model->validate();

        $responder = new ForDayResponder($this->dayService, $model->day, $model->month, $model->year, $model->site_id);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function getGraphicForUser(Request $request): JsonResponse
    {
        $model = new ForUserModel($request);
        $model->validate();

        $responder = new ForUserResponder($this->dayService, $model->month, $model->year);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function generatePdfForUser(Request $request) {
        $model = new ForUserModel($request);
        $model->validate();

        $data = $this->dayService->getUserDay($model->month, $model->year);

        view()->share('days', $data);

        $date = Carbon::createFromDate($model->year, $model->month, 1)->locale("pl");

        $dateString = $date->format('m/Y');

        view()->share('date', $dateString);
        $pdf = app('dompdf.wrapper');
        $pdf->loadView('pdf.pdf_dayForUser', $data);

        return $pdf->download('pdf_file.pdf');
    }

    /**
     * @return JsonResponse
     */
    public function synchronizeUserWithDay(): JsonResponse
    {
        $responder = new SynchronizeUserWithDayResponder();

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @return JsonResponse
     */
    public function deleteOldDays(): JsonResponse
    {
        $service = new DayService();

        $responder = new DeleteOldDaysResponder();

        return APIResponse::produceResponseFromResponder($responder);
    }
}
