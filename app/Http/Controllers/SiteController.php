<?php

namespace App\Http\Controllers;

use App\Http\Request\Site\AddSiteModel;
use App\Http\Request\Site\EditSiteModel;
use App\Http\Request\Site\SiteIDModel;
use App\Http\Resonders\Site\AddSiteResponder;
use App\Http\Resonders\Site\DeleteSiteResponder;
use App\Http\Resonders\Site\EditSiteResponder;
use App\Http\Resonders\Site\GetSiteResponder;
use App\Http\Resonders\Site\GetSitesResponder;
use App\Http\Response\APIResponse;
use App\Http\Service\SiteService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class SiteController extends Controller
{

    /**
     * @var SiteService
     */
    public $service;

    /**
     * SiteController constructor.
     */
    public function __construct()
    {
        $this->service = new SiteService();
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function addSite(Request $request): JsonResponse
    {

        $model = new AddSiteModel($request);
        $model->validate();

        $responder = new AddSiteResponder($model->name, $model->number, $this->service);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function deleteSite(Request $request): JsonResponse
    {
        $model = new SiteIDModel($request);
        $model->validate();

        $responder = new DeleteSiteResponder($model->site_id, $this->service);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function editSite(Request $request): JsonResponse
    {
        $model = new EditSiteModel($request);
        $model->validate();

        $responder = new EditSiteResponder($model->site_id, $model->number, $model->name, $this->service);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function getSite(Request $request): JsonResponse
    {
        $model = new SiteIDModel($request);
        $model->validate();

        $responder = new GetSiteResponder($model->site_id, $this->service);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getSites(Request $request): JsonResponse
    {
        $responder = new GetSitesResponder($this->service);

        return APIResponse::produceResponseFromResponder($responder);
    }
}
