<?php

namespace App\Http\Controllers;

use App\Http\Request\User\ChangePasswordModel;
use App\Http\Request\User\ChangeRoleModel;
use App\Http\Request\User\ModifyUserModel;
use App\Http\Request\User\RegisterUserModel;
use App\Http\Request\User\UserIDModal;
use App\Http\Resonders\User\ActiveNotificationsUserResponder;
use App\Http\Resonders\User\ActiveUserResponder;
use App\Http\Resonders\User\ChangePasswordResponder;
use App\Http\Resonders\User\ChangeRoleResponder;
use App\Http\Resonders\User\DeleteUserResponder;
use App\Http\Resonders\User\GetUserResponder;
use App\Http\Resonders\User\GetUsersResponder;
use App\Http\Resonders\User\ModifyUserResponder;
use App\Http\Resonders\User\RegisterUserResponder;
use App\Http\Resonders\User\UnActiveNotificationsUserResponder;
use App\Http\Resonders\User\UnActiveUserResponder;
use App\Http\Response\APIResponse;
use App\Http\Service\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{

    /**
     * @var UserService
     */
    public $service;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->service = new UserService();
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function registerUser(Request $request): JsonResponse
    {
        $model = new RegisterUserModel($request);
        $model->validate();

        $responder = new RegisterUserResponder($this->service, $model->firstName, $model->lastName, $model->password, $model->email, $model->site_id);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function activeUser(Request $request): JsonResponse
    {
        $model = new UserIDModal($request);
        $model->validate();

        $responder = new ActiveUserResponder($this->service, $model->user_id);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function unActiveUser(Request $request): JsonResponse
    {
        $model = new UserIDModal($request);
        $model->validate();

        $responder = new UnActiveUserResponder($this->service, $model->user_id);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function activeNotificationsUser(Request $request): JsonResponse
    {
        $model = new UserIDModal($request);
        $model->validate();

        $responder = new ActiveNotificationsUserResponder($this->service, $model->user_id);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function unActiveNotificationsUser(Request $request): JsonResponse
    {
        $model = new UserIDModal($request);
        $model->validate();

        $responder = new UnActiveNotificationsUserResponder($this->service, $model->user_id);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function deleteUser(Request $request): JsonResponse
    {
        $model = new UserIDModal($request);
        $model->validate();

        $responder = new DeleteUserResponder($this->service, $model->user_id);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function getUser(Request $request): JsonResponse
    {
        $model = new UserIDModal($request);
        $model->validate();

        $responder = new GetUserResponder($this->service, $model->user_id);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function changeRole(Request $request): JsonResponse
    {
        $model = new ChangeRoleModel($request);
        $model->validate();

        $responder = new ChangeRoleResponder($this->service, $model->user_id, $model->role);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function changePassword(Request $request): JsonResponse
    {
        $model = new ChangePasswordModel($request);
        $model->validate();

        $responder = new ChangePasswordResponder($this->service, $model->user_id, $model->password);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
 * @param Request $request
 * @return JsonResponse
 * @throws ValidationException
 */
    public function modifyUser(Request $request): JsonResponse
    {
        $model = new ModifyUserModel($request);
        $model->validate();

        $responder = new ModifyUserResponder($this->service, $model->user_id, $model->firstName, $model->lastName, $model->site_id);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @return JsonResponse
     */
    public function getUsers(): JsonResponse
    {
        $responder = new GetUsersResponder($this->service);

        return APIResponse::produceResponseFromResponder($responder);
    }




}
