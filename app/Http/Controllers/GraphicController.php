<?php

namespace App\Http\Controllers;

use App\Exceptions\DontHaveAccessToSite;
use App\Helpers\UserHelper;
use App\Http\Response\APIResponse;
use App\Http\Service\GraphicService;
use App\Jobs\UpdateGraphicJob;
use App\UpdateDays;
use Faker\Provider\File;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class GraphicController extends Controller
{

    public $service;

    /**
     * GraphicController constructor.
     */
    public function __construct()
    {
        $this->service = new GraphicService();
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws DontHaveAccessToSite
     */
    public function updateGraphic(Request $request) {
        $validator = Validator::make($request->all(),
            [
                'graphic' => 'required|mimes:pdf',
                'mgr' => 'required|mimes:pdf',
                'site_id' => 'nullable|numeric|exists:sites,id'
            ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $site_id = $request->get('site_id');

        if (!$site_id) {
            $site_id = UserHelper::getUserFromToken()->site_id;
        }

        UserHelper::checkAccessToSite($site_id);

        $random = Str::random(5);
        $fileName = $random . '.pdf';
        $fileNameMgr = $random . 'mgr.pdf';

        $file = $request->file('graphic')->storeAs('', $fileName, 'local');
        $fileMgr = $request->file('mgr')->storeAs('', $fileNameMgr, 'local');

        $url = storage_path('app/') . $file;
        $url1 = storage_path('app/') . $fileMgr;

        $update = new UpdateDays();
        $update->site_id = $site_id;


        UpdateGraphicJob::dispatch($url, 'persons', $site_id);
        UpdateGraphicJob::dispatch($url1, 'mgr', $site_id);

        return APIResponse::produceResponse(200);
    }

}
