<?php

namespace App\Http\Controllers;

use App\Http\Request\MonthPetition\CreateMonthPetitionModel;
use App\Http\Request\MonthPetition\ModifyMonthPetitionModel;
use App\Http\Request\MonthPetition\MonthPetitionIdModel;
use App\Http\Resonders\MonthPetition\ActiveMonthPetitionResponder;
use App\Http\Resonders\MonthPetition\CreateMonthPetitionResponder;
use App\Http\Resonders\MonthPetition\DeleteMonthPetitionResponder;
use App\Http\Resonders\MonthPetition\GetActiveMonthResponder;
use App\Http\Resonders\MonthPetition\GetMonthPetitionResponder;
use App\Http\Resonders\MonthPetition\GetMonthPetitionsResponder;
use App\Http\Resonders\MonthPetition\ModifyMonthPetitionResponder;
use App\Http\Resonders\MonthPetition\UnActiveMonthPetitionResponder;
use App\Http\Response\APIResponse;
use App\Http\Service\MonthPetitionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class MonthPetitionController extends Controller
{
    /**
     * @var MonthPetitionService
     */
    public $service;

    /**
     * MonthPetitionController constructor.
     */
    public function __construct()
    {
        $this->service = new MonthPetitionService();
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function createMonthPetition(Request $request): JsonResponse
    {
        $model = new CreateMonthPetitionModel($request);
        $model->validate();

        $responder = new CreateMonthPetitionResponder($this->service, $model->name);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function unActiveMonthPetition(Request $request): JsonResponse
    {
        $model = new MonthPetitionIdModel($request);
        $model->validate();

        $responder = new UnActiveMonthPetitionResponder($this->service, $model->month_petition_id);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function activeMonthPetition(Request $request): JsonResponse
    {
        $model = new MonthPetitionIdModel($request);
        $model->validate();

        $responder = new ActiveMonthPetitionResponder($this->service, $model->month_petition_id);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function modifyMonthPetition(Request $request): JsonResponse
    {
        $model = new ModifyMonthPetitionModel($request);
        $model->validate();

        $responder = new ModifyMonthPetitionResponder($this->service, $model->name, $model->month_petition_id);

        return APIResponse::produceResponseFromResponder($responder);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function getMonthPetition(Request $request): JsonResponse
    {
        $model = new MonthPetitionIdModel($request);
        $model->validate();

        $responder = new GetMonthPetitionResponder($this->service, $model->month_petition_id);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @return JsonResponse
     */
    public function getMonthPetitions(): JsonResponse
    {
        $responder = new GetMonthPetitionsResponder($this->service);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @return JsonResponse
     */
    public function getActiveMonthPetitions(): JsonResponse
    {
        $responder = new GetActiveMonthResponder($this->service);

        return APIResponse::produceResponseFromResponder($responder);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function deleteMonthPetitions(Request $request): JsonResponse
    {
        $model = new MonthPetitionIdModel($request);
        $model->validate();

        $responder = new DeleteMonthPetitionResponder($this->service, $model->month_petition_id);

        return APIResponse::produceResponseFromResponder($responder);
    }

}
