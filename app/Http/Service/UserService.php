<?php


namespace App\Http\Service;


use App\Exceptions\DontHaveAccess;
use App\Helpers\UserHelper;
use App\User;

class UserService
{

    public function registerUser(int $site_id, string $firstName, string $lastName, string $email, string $password) {
        $user = new User();
        $user->site_id = $site_id;
        $user->firstName = $firstName;
        $user->lastName = $lastName;
        $user->email = $email;
        $user->password = $password;

        $user->save();
    }

    public function activeUser(int $user_id) {
        $user = User::find($user_id);
        $user->active = 1;
        $user->save();
    }

    public function unActiveUser(int $user_id) {
        $user = User::find($user_id);
        $user->active = 0;
        $user->save();
    }

    public function activeNotificationsUser(int $user_id) {
        $user = User::find($user_id);
        $user->notifications = 1;
        $user->save();
    }

    public function unActiveNotificationsUser(int $user_id) {
        $user = User::find($user_id);
        $user->notifications = 0;
        $user->save();
    }

    public function changeRole(int $user_id, string $role) {
        $user = User::find($user_id);
        $user->role = $role;
        $user->save();
    }

    public function deleteUser(int $user_id) {
        $user = User::find($user_id);

        $days = $user->days;

        foreach ($days as $day) {
            $day->user_id = null;
            $day->save();
        }

        $user->delete();
    }

    public function changePassword(int $user_id, string $password) {
        $user = User::find($user_id);
        $user->password = $password;
        $user->save();
    }

    public function modifyUser(int $user_id, int $site_id, string $firstName, string $lastName) {
        $user = User::find($user_id);
        $user->site_id = $site_id;
        $user->firstName = $firstName;
        $user->lastName = $lastName;
        $user->save();
    }

    public function getUsers() {
        if(UserHelper::isSuAdmin()) {
            return User::orderBy('active', 'asc')->with(['site'])->get()->toArray();
        } else {
            $site_id = UserHelper::getUserFromToken()->site_id;
            return User::where('site_id', $site_id)->with(['site'])->orderBy('active', 'asc')->get()->toArray();
        }
    }

    /**
     * @param int $user_id
     * @return mixed
     * @throws DontHaveAccess
     */
    public function getUser(int $user_id) {

        $id = UserHelper::getUserIdFromToken();

        if ($id === $user_id || UserHelper::isAdmin()) {
            return User::where('id', $user_id)->with(['site'])->first();
        } else {
            throw new DontHaveAccess();
        }
    }
}