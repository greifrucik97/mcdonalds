<?php


namespace App\Http\Service;


use App\Site;
use Illuminate\Database\Eloquent\Collection;

class SiteService
{

    /**
     * @param int $number
     * @param string $name
     */
    public function addSite(int $number, string $name): void
    {
        $site = new Site();

        $site->number = $number;
        $site->name = $name;

        $site->save();
    }

    /**
     * @param int $id
     */
    public function deleteSite(int $id):void
    {
        $site = Site::find($id);

        $users = $site->users;

        foreach ($users as $user)
        {
            $user->site_id = null;
            $user->save();
        }

        $days = $site->days;

        foreach ($days as $day)
        {
            $day->site_id = null;
            $day->save();
        }

        $updateDays = $site->updateDays;

        foreach ($updateDays as $updateDay)
        {
            $updateDay->site_id = null;
            $updateDay->save();
        }

        $site->delete();
    }

    /**
     * @param int $id
     * @return Site
     */
    public function getSite(int $id): Site
    {
        return Site::find($id);
    }


    /**
     * @return Site[]|Collection
     */
    public function getSites()
    {
        return Site::all();
    }

    /**
     * @param int $site_id
     * @param int $number
     * @param string $name     */
    public function editSite(int $site_id, int $number, string $name): void
    {
        $site = Site::find($site_id);

        $site->number = $number;
        $site->name = $name;

        $site->save();
    }


}