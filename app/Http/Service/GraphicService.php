<?php


namespace App\Http\Service;


use App\Ghraphic;
use App\User;
use Carbon\Carbon;
use DateInterval;
use DateTime;

class GraphicService
{


    public function updateGraphic(string $path, string $type, int $site_id){
        $persons = [];
        $pierwsza = 1;
        $getDate = 0;
        $pdfParser = new \Smalot\PdfParser\Parser();
        $pdf = $pdfParser->parseFile($path);

        $pages  = $pdf->getPages();

        $rok = 0;
        $miesiac = 0;
        $miesiacNumber = 0;
        $lastday = 0;

        foreach ($pages as $page) {
            $text = $page->getText();

            $skuList = preg_split('/\r\n|\r|\n/', $text);

            $skuList = array_map('trim', $skuList);

            $skuList = array_filter($skuList, function($value) { return !is_null($value) && $value !== ''; });

            $skuList = array_filter($skuList, function ($value) { return $value !== "\t"; });

            $result = array_map('trim', $skuList);

            $split = explode(" ", $result[3]);

            if ($getDate === 0) {
                $rok = $split[4];
                $miesiac = $split[3];
                $getDate++;

                if ($miesiac === "Styczeń") {
                    $lastday = cal_days_in_month(CAL_GREGORIAN, 1, $rok);
                    $miesiacNumber = 1;
                } else if ($miesiac === "Luty") {
                    $lastday = cal_days_in_month(CAL_GREGORIAN, 2, $rok);
                    $miesiacNumber = 2;
                } else if ($miesiac === "Marzec") {
                    $lastday = cal_days_in_month(CAL_GREGORIAN, 3, $rok);
                    $miesiacNumber = 3;
                } else if ($miesiac === "Kwiecień") {
                    $lastday = cal_days_in_month(CAL_GREGORIAN, 4, $rok);
                    $miesiacNumber = 4;
                } else if ($miesiac === "Maj") {
                    $lastday = cal_days_in_month(CAL_GREGORIAN, 5, $rok);
                    $miesiacNumber = 5;
                } else if ($miesiac === "Czerwiec") {
                    $lastday = cal_days_in_month(CAL_GREGORIAN, 6, $rok);
                    $miesiacNumber = 6;
                } else if ($miesiac === "Lipiec") {
                    $lastday = cal_days_in_month(CAL_GREGORIAN, 7, $rok);
                    $miesiacNumber = 7;
                } else if ($miesiac === "Sierpień") {
                    $lastday = cal_days_in_month(CAL_GREGORIAN, 8, $rok);
                    $miesiacNumber = 8;
                } else if ($miesiac === "Wrzesień") {
                    $lastday = cal_days_in_month(CAL_GREGORIAN, 9, $rok);
                    $miesiacNumber = 9;
                } else if ($miesiac === "Październik") {
                    $lastday = cal_days_in_month(CAL_GREGORIAN, 10, $rok);
                    $miesiacNumber = 10;
                } else if ($miesiac === "Listopad") {
                    $lastday = cal_days_in_month(CAL_GREGORIAN, 11, $rok);
                    $miesiacNumber = 11;
                } else if ($miesiac === "Grudzień") {
                    $lastday = cal_days_in_month(CAL_GREGORIAN, 12, $rok);
                    $miesiacNumber = 12;
                }
            }

            $offset = 2 * $lastday + 4 + $pierwsza;
            $result = array_slice($result, $offset, count($result) - $offset);
            $pierwsza = 0;

            $add = 3 * $lastday + 3;

            for ($i = 0; $i < count($result); $i = $i + $add) {
                $array = array_slice($result, $i, $add);

                array_push($persons, $array);
            }
        }

        \App\Day::where('id', 'like', $rok . "-" . $miesiacNumber . "-" . $type . "%")->delete();


        $id = 1;
        foreach ($persons as $person) {
            $name = explode(" ", $person[0]);
            $firstName = $name[1];
            $lastName = $name[0];

            $days = [];

            $findUsers = User::where('firstName', $firstName)->where('lastName', $lastName)->first();

            if ($findUsers) {
                $user_id = $findUsers->id;
            } else {
                $user_id = null;
            }

            for($i = 1; $i <= $lastday; $i++) {

                $day = new \App\Day();
                $day->id=$rok . "-" . $miesiacNumber . "-" . $type . "-" . $id;
                $day->user_id = $user_id;
                $day->firstName = $firstName;
                $day->lastName = $lastName;
                $day->user_id = $user_id;
                $day->day = $i;
                $day->site_id = $site_id;


                if (preg_match('/([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]/', $person[$i])) {
                    $day->start = $person[$i];
                } else {
                    $day->start = "";
                }

                if ($person[2 + $lastday + $i] == "--") {
                    $day->end = "";
                    $day->numberHours = "";
                } else {
                    $day->end = $person[2 + $lastday + $i];
                    $numberHours = $this->timeDiff($day->start, $day->end);
                    $day->numberHours = $numberHours;
                }

                if ($person[2 + $lastday + $i + $lastday] == "--") {
                    $day->attention = "";
                } else {
                    $day->attention = $person[2 + $lastday + $i + $lastday];
                }

                if ($type == "mgr") {
                    $day->mgr = 1;
                } else {
                    $day->mgr = 0;
                }

                $day->year = $rok;
                $day->month = $miesiacNumber;
                $day->date = Carbon::createFromDate($rok, $miesiacNumber, $i)->format("Y-m-d");

                $day->save();
                $id++;
            }
        }
    }


    private function timeDiff(string $start, string $koniec)
    {
        try {
            $origin = new DateTime($start);
            $target = new DateTime($koniec);

            if ($origin > $target) {
                $target = $target->add(new DateInterval('P1D'));
            }

            $interval = $origin->diff($target);

            $min = $interval->i;
            $hours = $interval->h;

            if ($hours >= 6) {
                if ($min == 0) {
                    $hours = $hours - 1;
                    $min = 45;
                } else {

                    $min = $min - 15;
                }
            }

            if ($min == 15) {
                $min = "25";
            } else if ($min == 30) {
                $min = "50";
            } else if ($min == 45) {
                $min = "75";
            }

            if($min == 0) {
                return $hours;
            } else {
                return $hours . "," . $min;
            }


        } catch (\Exception $e) {
        }



    }




}