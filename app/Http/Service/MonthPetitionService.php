<?php


namespace App\Http\Service;


use App\Helpers\UserHelper;
use App\MonthPetition;
use App\Petition;

class MonthPetitionService
{

    public function createMonthPetition(string $name)
    {
        $monthPetition = new MonthPetition();

        $user = UserHelper::getUserFromToken();

        $monthPetition->site_id = $user->site_id;
        $monthPetition->name = $name;

        $monthPetition->save();
    }

    public function unActiveMonthPetition(int $id)
    {
        $monthPetition = MonthPetition::find($id);
        $monthPetition->active = 0;
        $monthPetition->save();
    }

    public function activeMonthPetition(int $id)
    {
        $monthPetition = MonthPetition::find($id);
        $monthPetition->active = 1;
        $monthPetition->save();
    }

    public function modifyMonthPetition(int $id, string $name)
    {
        $monthPetition = MonthPetition::find($id);
        $monthPetition->name = $name;
        $monthPetition->save();
    }

    public function getMonthPetition(int $id)
    {
        $user = UserHelper::getUserFromToken();
        if (UserHelper::isSuAdmin()) {
            return MonthPetition::where('id', $id)->with(['petitions'])->first();
        } else {
            return MonthPetition::where('id', $id)->where('site_id', $user->site_id)->with(['petitions'])->first();
        }
    }

    public function getMonthPetitions(): array
    {
        $user = UserHelper::getUserFromToken();
        if (UserHelper::isSuAdmin()) {
            return MonthPetition::with(['petitions'])->get()->toArray();
        } else {
            return MonthPetition::where('site_id', $user->site_id)->with(['petitions'])->get()->toArray();
        }
    }

    public function getActiveMonth(): array
    {
        $user = UserHelper::getUserFromToken();
        if (UserHelper::isSuAdmin()) {
            return MonthPetition::where('active', 1)->with(['petitions'])->get()->toArray();
        } else {
            return MonthPetition::where('active', 1)->where('site_id', $user->site_id)->with(['petitions'])->get()->toArray();
        }
    }

    public function deleteMonthPetitions(int $id)
    {
        $monthPetition = MonthPetition::find($id);

        $petitions = $monthPetition->petitions;

        Petition::destroy($petitions);

        $monthPetition->delete();
    }

}