<?php

namespace App\Http\Service;


use App\Helpers\UserHelper;
use App\Petition;

class PetitionService
{

    public function createPetition(int $month_petition_id, string $content)
    {
        $user = UserHelper::getUserFromToken();

        $petition = new Petition();
        $petition->user_id = $user->id;
        $petition->month_petition_id = $month_petition_id;
        $petition->content = $content;

        $petition->save();
    }

    public function deletePetition(int $id)
    {
        $petition = Petition::find($id);
        $petition->delete();
    }

    public function modifyPetition(int $id, string $content)
    {
        $petition = Petition::find($id);
        $petition->content = $content;
        $petition->save();
    }

    public function getPetition(int $id)
    {
        return Petition::where('id', $id)->first();
    }

    public function getUserPetitionsToMonthPetitions(int $month_petition_id)
    {
        return Petition::where('month_petition_id', $month_petition_id)->get()->toArray();
    }

}