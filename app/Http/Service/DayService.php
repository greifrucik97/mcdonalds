<?php


namespace App\Http\Service;


use App\Day;
use App\Helpers\UserHelper;
use App\User;
use Carbon\Carbon;

class DayService
{

    public function getForDay(int $day, int $month, int $year, ?int $site_id)
    {
        $days = [];
        $user = User::find(UserHelper::getUserIdFromToken());

        if ($site_id) {
            if (UserHelper::isMgr()) {
                $days = Day::where('day', $day)->where('month', $month)->where('year', $year)->where("start", "!=", "")->where('site_id', $site_id)->orderBy('start')->get();
            } else {
                $days = Day::where('day', $day)->where('month', $month)->where('year', $year)->where("start", "!=", "")->where('site_id', $site_id)->where('mgr', 0)->orderBy('start')->get();
            }

        } else {
            if (UserHelper::isMgr()) {
                $days = Day::where('day', $day)->where('month', $month)->where('year', $year)->where("start", "!=", "")->where('site_id', $user->site_id)->orderBy('start')->get();
            } else {
                $days = Day::where('day', $day)->where('month', $month)->where('year', $year)->where("start", "!=", "")->where('site_id', $user->site_id)->where('mgr', 0)->orderBy('start')->get();
            }
        }
        $emps = [];
        $mgrs = [];



        foreach ($days as $day) {
            if ($day->mgr == 1) {
                array_push($mgrs, $day);
            } else {
                array_push($emps, $day);
            }
        }

        return [
            'employers' => $emps,
            'mgrs' => $mgrs
        ];

    }

    public function getUserDay(int $month, int $year)
    {
        return UserHelper::getUserFromToken()->days()->where('year', $year)->where('month', $month)->orderBy('day', 'asc')->get()->toArray();
    }

    public function synchronizeUserWithDay(?int $site_id)
    {
        if ($site_id) {
            $freeDays = Day::where('site_id', $site_id)->where('user_id', null)->get();
        } else {
            $freeDays = Day::where('user_id', null)->get();
        }

        foreach ($freeDays as $day) {

            $user = User::where('firstName', $day->firstName)->where('lastName', $day->lastName)->first();

            if ($user) {
                $day->user_id = $user->id;
                $day->save();
            }
        }
    }

    public function deleteOldDays(?int $site_id)
    {
        $date = Carbon::now()->subMonth(9)->format("Y-m-d");

        if ($site_id) {
            $freeDays = Day::where('site_id', $site_id)->whereDate('date', '<', $date)->get();
        } else {
            $freeDays = Day::whereDate('date', '<', $date)->get();
        }

        Day::destroy($freeDays);
    }

}