<?php


namespace App\Http\Resonders\User;


use App\Http\Response\Responder;
use App\Http\Service\UserService;

class ChangePasswordResponder implements Responder
{
    /**
     * @var UserService
     */
    public $userService;
    /**
     * @var int
     */
    public $user_id;
    /**
     * @var string
     */
    public $password;


    /**
     * ChangePasswordResponder constructor.
     * @param UserService $userService
     * @param int $user_id
     * @param string $password
     */
    public function __construct(UserService $userService, int $user_id, string $password)
    {
        $this->userService = $userService;
        $this->user_id = $user_id;
        $this->password = $password;
    }

    public function generate(): array
    {

        $this->userService->changePassword($this->user_id, $this->password);

        return [];
    }
}