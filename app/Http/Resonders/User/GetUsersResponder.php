<?php


namespace App\Http\Resonders\User;


use App\Http\Response\Responder;
use App\Http\Service\UserService;

class GetUsersResponder implements Responder
{

    /**
     * @var UserService
     */
    public $userService;

    /**
     * GetUsersResponder constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function generate(): array
    {
        return $this->userService->getUsers();
    }

}