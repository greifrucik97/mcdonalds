<?php


namespace App\Http\Resonders\User;

use App\Http\Response\Responder;
use App\Http\Service\UserService;

class RegisterUserResponder implements Responder
{

    /**
     * @var UserService
     */
    public $userService;
    /**
     * @var string
     */
    public $firstName;
    /**
     * @var string
     */
    public $lastName;
    /**
     * @var string
     */
    public $password;
    /**
     * @var string
     */
    public $email;
    /**
     * @var int
     */
    public $site_id;


    /**
     * RegisterUserResponder constructor.
     * @param UserService $userService
     * @param string $firstName
     * @param string $lastName
     * @param string $password
     * @param string $email
     * @param int $site_id
     */
    public function __construct(UserService $userService, string $firstName, string $lastName, string $password, string $email, int $site_id)
    {

        $this->userService = $userService;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->password = $password;
        $this->email = $email;
        $this->site_id = $site_id;

    }

    public function generate(): array
    {
        $this->userService->registerUser($this->site_id, $this->firstName, $this->firstName, $this->email, $this->password);

        return [];
    }

}