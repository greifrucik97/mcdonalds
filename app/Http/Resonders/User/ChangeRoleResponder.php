<?php


namespace App\Http\Resonders\User;


use App\Http\Response\Responder;
use App\Http\Service\UserService;

class ChangeRoleResponder implements Responder
{
    /**
     * @var UserService
     */
    public $userService;
    /**
     * @var int
     */
    public $user_id;
    /**
     * @var string
     */
    public $role;


    /**
     * ChangeRoleResponder constructor.
     * @param UserService $userService
     * @param int $user_id
     * @param string $role
     */
    public function __construct(UserService $userService, int $user_id, string $role)
    {
        $this->userService = $userService;
        $this->user_id = $user_id;
        $this->role = $role;
    }

    public function generate(): array
    {

        $this->userService->changeRole($this->user_id, $this->role);

        return [];
    }
}