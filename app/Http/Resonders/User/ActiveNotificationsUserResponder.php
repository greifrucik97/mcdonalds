<?php


namespace App\Http\Resonders\User;


use App\Http\Response\Responder;
use App\Http\Service\UserService;

class ActiveNotificationsUserResponder implements Responder
{
    /**
     * @var UserService
     */
    public $userService;
    /**
     * @var int
     */
    public $user_id;


    /**
     * ActiveNotificationsUserResponder constructor.
     * @param UserService $userService
     * @param int $user_id
     */
    public function __construct(UserService $userService, int $user_id)
    {
        $this->userService = $userService;
        $this->user_id = $user_id;
    }

    public function generate(): array
    {

        $this->userService->activeNotificationsUser($this->user_id);

        return [];
    }

}