<?php


namespace App\Http\Resonders\User;


use App\Http\Response\Responder;
use App\Http\Service\UserService;

class ModifyUserResponder implements Responder
{

    /**
     * @var UserService
     */
    public $userService;
    /**
     * @var string
     */
    public $firstName;
    /**
     * @var string
     */
    public $lastName;
    /**
     * @var int
     */
    public $site_id;
    /**
     * @var int
     */
    public $user_id;


    /**
     * ModifyUserResponder constructor.
     * @param UserService $userService
     * @param int $user_id
     * @param string $firstName
     * @param string $lastName
     * @param int $site_id
     */
    public function __construct(UserService $userService, int $user_id, string $firstName, string $lastName, int $site_id)
    {
        $this->user_id = $user_id;
        $this->userService = $userService;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->site_id = $site_id;

    }

    public function generate(): array
    {
        $this->userService->modifyUser($this->user_id, $this->site_id, $this->firstName, $this->lastName);

        return [];
    }

}