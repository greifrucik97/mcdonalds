<?php


namespace App\Http\Resonders\MonthPetition;


use App\Http\Response\Responder;
use App\Http\Service\MonthPetitionService;

class CreateMonthPetitionResponder implements Responder
{
    /**
     * @var MonthPetitionService
     */
    public $servie;
    /**
     * @var string
     */
    public $name;


    /**
     * CreateMonthPetitionResponder constructor.
     * @param MonthPetitionService $service
     * @param string $name
     */
    public function __construct(MonthPetitionService $service, string $name)
    {
        $this->servie = $service;
        $this->name = $name;
    }

    public function generate(): array
    {
        $this->servie->createMonthPetition($this->name);
        return [];
    }

}