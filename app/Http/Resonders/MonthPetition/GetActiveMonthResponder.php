<?php


namespace App\Http\Resonders\MonthPetition;


use App\Http\Response\Responder;
use App\Http\Service\MonthPetitionService;

class GetActiveMonthResponder implements Responder
{

    /**
     * @var MonthPetitionService
     */
    public $service;


    /**
     * GetMonthPetitionsResponder constructor.
     * @param MonthPetitionService $service
     */
    public function __construct(MonthPetitionService $service)
    {
        $this->service = $service;
    }

    public function generate(): array
    {

        return $this->service->getActiveMonth();
    }

}