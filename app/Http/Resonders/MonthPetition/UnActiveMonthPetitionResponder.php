<?php


namespace App\Http\Resonders\MonthPetition;


use App\Http\Response\Responder;
use App\Http\Service\MonthPetitionService;

class UnActiveMonthPetitionResponder implements Responder
{

    /**
     * @var MonthPetitionService
     */
    public $service;
    /**
     * @var int
     */
    public $id;


    /**
     * CreateMonthPetitionResponder constructor.
     * @param MonthPetitionService $service
     * @param int $id
     */
    public function __construct(MonthPetitionService $service, int $id)
    {
        $this->service = $service;
        $this->id = $id;
    }

    public function generate(): array
    {
        $this->service->unActiveMonthPetition($this->id);
        return [];
    }

}