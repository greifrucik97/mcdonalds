<?php


namespace App\Http\Resonders\MonthPetition;


use App\Http\Response\Responder;
use App\Http\Service\MonthPetitionService;

class ModifyMonthPetitionResponder implements Responder
{

    /**
     * @var MonthPetitionService
     */
    public $servie;
    /**
     * @var string
     */
    public $name;
    /**
     * @var int
     */
    public $id;


    /**
     * CreateMonthPetitionResponder constructor.
     * @param MonthPetitionService $service
     * @param string $name
     * @param int $id
     */
    public function __construct(MonthPetitionService $service, string $name, int $id)
    {
        $this->servie = $service;
        $this->name = $name;
        $this->id = $id;
    }

    public function generate(): array
    {
        $this->servie->modifyMonthPetition($this->id, $this->name);
        return [];
    }

}