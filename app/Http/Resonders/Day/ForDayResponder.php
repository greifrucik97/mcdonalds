<?php


namespace App\Http\Resonders\Day;


use App\Exceptions\DontHaveAccessToSite;
use App\Helpers\UserHelper;
use App\Http\Response\Responder;
use App\Http\Service\DayService;

class ForDayResponder implements Responder
{
    /**
     * @var DayService
     */
    public $dayService;
    /**
     * @var int
     */
    public $month;
    /**
     * @var int
     */
    public $day;
    /**
     * @var int
     */
    public $year;
    /**
     * @var int|null
     */
    public $site_id;


    /**
     * ForDayResponder constructor.
     * @param DayService $dayService
     * @param int $day
     * @param int $month
     * @param int $year
     * @param int|null $site_id
     */
    public function __construct(DayService $dayService, int $day, int $month, int $year, ?int $site_id)
    {
        $this->dayService = $dayService;
        $this->day = $day;
        $this->month = $month;
        $this->year =  $year;
        $this->site_id = $site_id;
    }

    /**
     * @return array
     * @throws DontHaveAccessToSite
     */
    public function generate(): array
    {
        UserHelper::checkAccessToSite($this->site_id);

        $days = $this->dayService->getForDay($this->day, $this->month, $this->year, $this->site_id);

        return $days;
    }

}