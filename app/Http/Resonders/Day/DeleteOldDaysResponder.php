<?php


namespace App\Http\Resonders\Day;


use App\Helpers\UserHelper;
use App\Http\Response\Responder;
use App\Jobs\DeleteOldDaysJob;
use App\Jobs\SynchronizeUsersWithDayJob;

class DeleteOldDaysResponder implements Responder
{

    public function generate(): array
    {
        if (UserHelper::isSuAdmin() ) {
            DeleteOldDaysJob::dispatch(null);
        } else {
            $user = UserHelper::getUserFromToken();
            DeleteOldDaysJob::dispatch($user->site_id);
        }
        return [];
    }


}