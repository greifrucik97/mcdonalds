<?php


namespace App\Http\Resonders\Day;


use App\Exceptions\DontHaveAccessToSite;
use App\Helpers\UserHelper;
use App\Http\Response\Responder;
use App\Http\Service\DayService;

class ForUserResponder implements Responder
{
    /**
     * @var DayService
     */
    public $dayService;
    /**
     * @var int
     */
    public $month;
    /**
     * @var int
     */
    public $year;


    /**
     * ForUserResponder constructor.
     * @param DayService $dayService
     * @param int $month
     * @param int $year
     */
    public function __construct(DayService $dayService, int $month, int $year)
    {
        $this->dayService = $dayService;
        $this->month = $month;
        $this->year =  $year;
    }

    /**
     * @return array
     */
    public function generate(): array
    {

        $days = $this->dayService->getUserDay($this->month, $this->year);

        return $days;
    }

}