<?php


namespace App\Http\Resonders\Day;


use App\Helpers\UserHelper;
use App\Http\Response\Responder;
use App\Http\Service\DayService;
use App\Jobs\SynchronizeUsersWithDayJob;

class SynchronizeUserWithDayResponder implements Responder
{


    public function generate(): array
    {

        if (UserHelper::isSuAdmin()) {
            SynchronizeUsersWithDayJob::dispatch(null);
        } else {
            $user = UserHelper::getUserFromToken();
            SynchronizeUsersWithDayJob::dispatch($user->site_id);
        }
        return [];
    }
}