<?php


namespace App\Http\Resonders\Site;


use App\Http\Response\Responder;
use App\Http\Service\SiteService;

class EditSiteResponder implements Responder
{
    /**
     * @var int
     */
    public $number;

    /**
     * @var int
     */
    public $site_id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var SiteService
     */
    public $service;



    /**
     * EditSiteResponder constructor.
     * @param mixed $site_id
     * @param mixed $number
     * @param mixed $name
     * @param SiteService $service
     */
    public function __construct(int $site_id, int $number, string $name, SiteService $service)
    {
        $this->site_id = $site_id;
        $this->number = $number;
        $this->name = $name;
        $this->service = $service;
    }

    public function generate(): array
    {
        $this->service->editSite($this->site_id, $this->number, $this->name);
        return [];
    }

}