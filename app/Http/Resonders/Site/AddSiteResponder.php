<?php


namespace App\Http\Resonders\Site;


use App\Http\Response\Responder;
use App\Http\Service\SiteService;

class AddSiteResponder implements Responder
{

    /**
     * @var int
     */
    public $number;

    /**
     * @var string
     */
    public $name;

    /**
     * @var SiteService
     */
    public $service;

    /**
     * AddSiteResponder constructor.
     * @param string $name
     * @param int $number
     * @param SiteService $siteService
     */
    public function __construct(string $name, int $number, SiteService $siteService)
    {
        $this->number = $number;
        $this->name = $name;
        $this->service = $siteService;
    }


    /**
     * @return array
     */
    public function generate(): array
    {
        $this->service->addSite($this->number, $this->name);

        return [];
    }

}