<?php


namespace App\Http\Resonders\Site;


use App\Http\Response\Responder;
use App\Http\Service\SiteService;

class GetSiteResponder implements Responder
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var SiteService
     */
    public $service;


    /**
     * DeleteSiteResponder constructor.
     * @param int $id
     * @param SiteService $siteService
     */
    public function __construct(int $id, SiteService $siteService)
    {
        $this->id = $id;
        $this->service = $siteService;
    }


    /**
     * @return array
     */
    public function generate(): array
    {
       return [
           $this->service->getSite($this->id)
       ];
    }

}