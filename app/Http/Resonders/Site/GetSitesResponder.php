<?php


namespace App\Http\Resonders\Site;


use App\Http\Response\Responder;
use App\Http\Service\SiteService;

class GetSitesResponder implements Responder
{

    /**
     * @var SiteService
     */
    public $service;

    /**
     * DeleteSiteResponder constructor.
     * @param SiteService $siteService
     */
    public function __construct(SiteService $siteService)
    {
        $this->service = $siteService;
    }


    public function generate(): array
    {
        return $this->service->getSites()->toArray();
    }

}