<?php

namespace App\Http\Middleware;


use App\Domain\Helpers\ReturnStatus;
use App\Helpers\UserHelper;
use App\Http\Response\APIResponse;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param array $roles
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$roles)
    {

        $user = auth()->user();
        if (UserHelper::isSuAdmin()) {
            return $next($request);
        }

        if (UserHelper::isAdmin() && (in_array(UserHelper::ADMIN, $roles) || in_array(UserHelper::MGR, $roles) || in_array(UserHelper::INSRTUCTOR, $roles) || in_array(UserHelper::EMPLOYER, $roles))) {
            return $next($request);
        }

        if (UserHelper::isMgr() && (in_array(UserHelper::MGR, $roles) || in_array(UserHelper::INSRTUCTOR, $roles) || in_array(UserHelper::EMPLOYER, $roles))) {
            return $next($request);
        }
        if (UserHelper::isInstructor() && (in_array(UserHelper::INSRTUCTOR, $roles) || in_array(UserHelper::EMPLOYER, $roles))) {
            return $next($request);
        }

        if (UserHelper::isEmployer() && in_array(UserHelper::EMPLOYER, $roles)) {
            return $next($request);
        }

        return APIResponse::produceResponse(ReturnStatus::BAD_ROLE);
    }
}
