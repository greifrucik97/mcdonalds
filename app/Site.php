<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int number
 * @property string name
 */
class Site extends Model
{

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function days()
    {
        return $this->hasMany(Day::class);
    }

    public function updateDays()
    {
        return $this->hasMany(UpdateDays::class);
    }



}
