<?php

namespace App\Jobs;

use App\Http\Service\DayService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteOldDaysJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int|null
     */
    public $site_id;


    /**
     * DeleteOldDaysJob constructor.
     * @param int|null $site_id
     */
    public function __construct(?int $site_id)
    {
        $this->site_id = $site_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $service = new DayService();

        $service->deleteOldDays($this->site_id);
    }
}
