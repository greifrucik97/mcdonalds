<?php

namespace App\Jobs;

use App\Http\Service\GraphicService;
use App\UpdateDays;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class UpdateGraphicJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    public $url;

    public $type;

    public $site_id;

    public $service;

    /**
     * UpdateGraphicJob constructor.
     * @param string $url
     * @param string $type
     * @param int $site_id
     */
    public function __construct(string $url, string $type, int $site_id)
    {
        $this->url = $url;
        $this->type = $type;
        $this->site_id = $site_id;
        $this->service = new GraphicService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $this->service->updateGraphic($this->url, $this->type, $this->site_id);

        Storage::delete([
            $this->url
        ]);
    }
}
