<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class init extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('command:Clear');
        $this->call('command:MakeAdminAccount');
        $this->call('command:CreateSite');
        $this->call('command:EmployerAccount');
        $this->call('command:MgrAccount');
        $this->call('command:AdminAccount');

    }
}
