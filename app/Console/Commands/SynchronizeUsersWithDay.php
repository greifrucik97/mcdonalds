<?php

namespace App\Console\Commands;

use App\Http\Service\DayService;
use App\Jobs\SynchronizeUsersWithDayJob;
use Illuminate\Console\Command;

class SynchronizeUsersWithDay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:synchronizeUsersWithDay';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        SynchronizeUsersWithDayJob::dispatch(null);

        return true;
    }
}
