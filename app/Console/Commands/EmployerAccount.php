<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class EmployerAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:EmployerAccount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = new User();

        $user->site_id = 1;
        $user->firstName = 'Gracjan';
        $user->lastName = 'Młynarczyk';
        $user->email = 'gracjan@gracjan.pl';
        $user->password = 'gracjan';
        $user->role = 'employer';
        $user->active = 1;
        $user->notifications = 1;

        $user->save();

        return true;
    }
}
