<?php

namespace App\Console\Commands;

use App\Site;
use Illuminate\Console\Command;

class CreateSite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:CreateSite';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $site = new Site();

        $site->number = 84;
        $site->name = "Zielona Góra 1";

        $site->save();
    }
}
