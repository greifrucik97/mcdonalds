<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('site_id')->nullable();
            $table->string('firstName');
            $table->string('lastName');
            $table->enum('role', ['suadmin', 'instructor', 'admin', 'mgr', 'employer'])->default('employer');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('active')->default(0);
            $table->integer('notifications')->default(0);
            $table->timestamps();
            $table->foreign('site_id')->references('id')->on('sites');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
